import { Knex } from 'knex'

export class CompanyService {
  constructor(public knex: Knex) {}

  getCompanyList() {
    return this.knex.select('*').from('company')
  }

  createPost(company_id: number, content: string) {
    return this.knex
      .insert({ company_id, content })
      .into('post')
      .returning('id')
  }
}
