import { Knex } from 'knex'

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex('post').del()
  await knex('company').del()

  // Inserts seed entries
  await knex('company').insert([
    { name: "Alice's Company" },
    { name: 'B & C Limited' },
  ])
}
