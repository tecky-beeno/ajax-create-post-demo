console.log('company-list.js')

let main = document.querySelector('main')

async function loadData() {
  main.innerHTML = '<p>Loading...</p>'
  let res = await fetch('/company/all')
  let data = await res.json()
  console.log('data:', data)
  if (typeof data === 'object') {
    console.table(data)
  }
  let companyList = data.data
  let html = '<ul>'
  companyList.forEach(
    company =>
      (html += `<li><a href="/company-detail.html?id=${company.id}">${company.name}<a/></li>`),
  )
  html += '</ul>'
  main.innerHTML = html
}

loadData()
