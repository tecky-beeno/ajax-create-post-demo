console.log('company-detail.js')

let main = document.querySelector('main')

async function loadData() {
  main.innerHTML = '<p>Loading...</p>'
  let params = new URLSearchParams(location.search)
  let id = params.get('id')
  let res = await fetch(`/company/${id}/post`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ content: 'Random note ' + Date.now() }),
  })
  let data = await res.json()
  console.log('data:', data)
  if (typeof data === 'object') {
    console.table(data)
  }
  main.innerHTML = JSON.stringify(data)
}

loadData()
