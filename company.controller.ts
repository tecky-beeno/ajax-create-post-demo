import { CompanyService } from './company.service'
import { Request, Response, Router } from 'express'

export class CompanyController {
  constructor(public companyService: CompanyService) {}

  toRouter() {
    let router = Router()
    router.get('/company/all', this.getCompanyList)
    router.post('/company/:id/post', this.createPost)
    return router
  }

  getCompanyList = (req: Request, res: Response) => {
    this.companyService
      .getCompanyList()
      .then(data => res.json({ data }))
      .catch(error => res.status(500).json({ error: error.toString() }))
  }

  createPost = (req: Request, res: Response) => {
    let company_id = parseInt(req.params.id)
    let content = req.body.content

    if (!company_id) {
      res
        .status(400)
        .json({ error: 'expect numeric company id in req.params.id' })
      return
    }
    if (!content || typeof content !== 'string') {
      res
        .status(400)
        .json({ error: 'expect non-empty content in req.body.content' })
      return
    }
    this.companyService
      .createPost(company_id, content)
      .then(data => res.json({ data }))
      .catch(error => res.status(500).json({ error: error.toString() }))
  }
}
