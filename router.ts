import express from 'express'
import { CompanyController } from './company.controller'
import { CompanyService } from './company.service'
import { knex } from './db'

export let router = express.Router()

router.get('/session', (req, res) => {
  res.json(req.session)
})

let companyService = new CompanyService(knex)
let companyController = new CompanyController(companyService)

router.use(companyController.toRouter())
