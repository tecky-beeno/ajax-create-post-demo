import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('company', t => {
    t.increments()
    t.string('name')
  })
  await knex.schema.createTable('post', t => {
    t.increments()
    t.string('content')
    t.integer('company_id').notNullable().references('company.id')
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('post')
  await knex.schema.dropTable('company')
}
