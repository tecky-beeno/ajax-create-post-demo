import { config } from 'dotenv'
import Knex from 'knex'
config()

let configs = require('./knexfile')
let mode = process.env.NODE_ENV || 'development'
export let knex = Knex(configs[mode])
